from google.cloud import storage


class GoogleStorage:

    def __init__(self):
        self.storage_client = storage.Client()

    def list_blob(self, bucket_name):
        return self.storage_client.get_bucket(bucket_name).list_blobs()

    def upload_blob(self, bucket_name, source_file_name, destination_blob_name):
        """Uploads a file to the bucket."""

        bucket = self.storage_client.get_bucket(bucket_name)
        blob = bucket.blob(destination_blob_name)
        blob.upload_from_filename(source_file_name)

        print('File {} uploaded to {}.'.format(
            source_file_name,
            destination_blob_name))
